import subprocess
import argparse
from textwrap import dedent
from os.path import basename


def read2bam(header, seq, bam_headers, tmp_sam, tmp_bam):
    with open(tmp_sam, 'w') as sam:
        h = header.strip('\n')[1:]
        ref = h.split('_')[0]
        sam.write('%s\t%s\t%s\n' % (
            '@SQ', 'SN:' + ref, 'LN:' + str(bam_headers[ref])
        ))
        rname = h
        start = int(h.split('_')[3])+1
        matches = str(len(seq))
        readQ = '?' * len(seq)
        sam.write(
            '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (
                rname, '0', ref, start, '60', matches + 'M',
                '*', '0', '0', seq, readQ
            ))
    bam = subprocess.run(['samtools', 'view', '-b', tmp_sam, '-o', tmp_bam])
    print(bam)
    return


def main():
    parser = argparse.ArgumentParser(
        prog='gfa-memsim',
        description=dedent("""
            Simulate reads from the sequences represented by paths in pangenome
            and report the position of their MEMs in graph coordinates. This
            feature is intended as a ground truth for benchmarking of
            algorithms which search MEMs in the pangenomic graphs.
        """).strip("\n"),
        epilog=""
    )

    parser.add_argument('-g', '--gfa', required=True)
    parser.add_argument('-c', '--cache', default='data/cache/')
    parser.add_argument('-o', '--output', default='data/results/')
    args = parser.parse_args()

    # mkdir data/cache
    name = basename(args.gfa)
    print(args.gfa)
    subprocess.run(['mkdir', '-p', args.cache])
    subprocess.run(['mkdir', '-p', args.output])

    # gfa to fasta
    fasta = args.cache + name + '.fa'
    cmd = ['gfatk', 'path', '--all', args.gfa]
    with open(fasta, 'w') as f:
        subprocess.run(cmd, stdout=f)

    # reads from fasta
    reads = args.output + name
    cmd = [
        'iss', 'generate', '--genomes', fasta, '--output', reads,
        '--model', 'NovaSeq', '--n_reads', '20'
    ]
    subprocess.run(cmd)
    reads_r1 = reads + '_R1.fastq'
    # reads_r2 = reads + '_R2.fastq' # TODO

    r1_fasta = reads_r1 + '.fa'
    cmd = ['sed', '-n', '1~4s/^@/>/p;2~4p', reads_r1]
    with open(r1_fasta, 'w') as f:
        subprocess.run(cmd, stdout=f)

    # create bam headers
    bam_headers = {}
    with open(fasta, 'r') as refs:
        for line in refs:
            if line.startswith('>'):
                key = line.strip('\n')[1:]
                bam_headers[key] = ''
            bam_headers[key] = len(line.strip('\n'))

    # gfa = 'data/inputs/sorted-toy-graph.gfa'

    tmp_sam = args.cache + 'tmp-read.sam'
    tmp_bam = args.cache + 'tmp-read.bam'
    tmp_bed = args.cache + 'tmp-ref-genome.bed'
    tmp_ref = args.cache + 'tmp-ref-genome.fa'
    tmp_seq = args.cache + 'tmp-seq-read.fa'
    tmp_mem = args.cache + 'tmp-rmems.txt'

    # sim_reads = 'data/inputs/exact-sim-iss-reads.fa'
    # graph_mems = 'data/results/mems-on-genomes.gaf'
    graph_mems = args.output + name + '.gaf'
    with open(r1_fasta, 'r') as reads, open(graph_mems, 'w') as gmems:
        for line in reads:
            if line.startswith('>'):
                header = line
                h = line.strip('\n').split('_')
                genome = h[0][1:]
                start = h[3]
                end = str(int(h[4])+1)
                continue
            read = line.strip('\n')
            read2bam(header, read, bam_headers, tmp_sam, tmp_bam)
            cmd = 'tools/gfainject --gfa ' + args.gfa + ' --bam ' + tmp_bam
            gfainject = subprocess.check_output(cmd, shell=True)\
                .decode().split()

            with open(tmp_bed, 'w') as f:
                subprocess.run(['echo', f"{genome}\t{start}\t{end}"], stdout=f)
            with open(tmp_ref, 'w') as f:
                subprocess.run(
                    ['bedtools', 'getfasta', '-fi', fasta, '-bed', tmp_bed],
                    stdout=f
                )
            with open(tmp_seq, 'w') as f:
                subprocess.run(['echo', f"{header}{read}"], stdout=f)
            with open(tmp_mem, 'w') as f:
                # cmd = ['tools/e-mem', tmp_ref, tmp_seq, '-l', '2', '-n']
                cmd = ['tools/e-mem', tmp_ref, tmp_seq, '-n']
                print(" ".join(cmd))
                subprocess.run(cmd, stdout=f)
            with open(tmp_mem, 'r') as rmems:
                for mem in rmems:
                    if mem.startswith('>'):
                        continue

                    m = mem.split()
                    length = int(m[2])
                    sgenome = int(m[0])-1

                    query_sequence_name = gfainject[0]
                    query_sequence_length = length
                    query_start = 0
                    query_end = length
                    strand = gfainject[4]
                    path = gfainject[5]
                    path_length = gfainject[6]
                    start_position_on_path = int(gfainject[7]) + sgenome
                    end_position_on_path = start_position_on_path + length
                    matches = length
                    alignment_block_length = length
                    mapping_quality = 60
                    gmems.write(
                        '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (
                            query_sequence_name, query_sequence_length,
                            query_start, query_end, strand,
                            path, path_length,
                            start_position_on_path, end_position_on_path,
                            matches, alignment_block_length, mapping_quality
                        )
                    )

# remove all tmp files - d
# subprocess.run(['rm tmp-*'], shell=True)
