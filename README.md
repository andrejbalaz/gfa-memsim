# GFA memsim

https://github.com/HadrienG/InSilicoSeq

in iss/generator.py
line: 134: id='%s_%s_%s_%s_%s/1' % (header, i, cpu_number, forward_start, forward_end)
line: 157: id='%s_%s_%s_%s_%s/2' % (header, i, cpu_number, reverse_start, reverse_end)

https://github.com/lucian-ilie/E-MEM
https://github.com/chfi/gfainject
https://github.com/tolkit/gfatk

## Install
```
conda -c fmfi-compbio gfa-memsim
```

```
mamba create -n memsim -f env.yml
sudo pacman -S boost
make
pip install -e .
```

## Run
```
gfa-memsim --gfa <graph.gfa> > <output.gaf>
```
